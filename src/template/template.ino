char t;
 
void setup() {
pinMode(13,OUTPUT);   //motor esquerdo para frente
pinMode(12,OUTPUT);   //motor esquerdo para tras
pinMode(11,OUTPUT);   //motor direito para frente
pinMode(10,OUTPUT);   //motor direito para tras
pinMode(9,OUTPUT);   //Led
Serial.begin(9600);
 
}
 
void loop() {
if(Serial.available()){
  t = Serial.read();
  Serial.println(t);
}
 
if(t == 'F'){            //move para frente
  digitalWrite(13,HIGH);
  digitalWrite(11,HIGH);
}
 
else if(t == 'B'){      //move para tras
  digitalWrite(12,HIGH);
  digitalWrite(10,HIGH);
}
 
else if(t == 'L'){      //vira para direita
  digitalWrite(11,HIGH);
}
 
else if(t == 'R'){      //virar para esquerda
  digitalWrite(13,HIGH);
}

else if(t == 'W'){    //liga e desliga o led
  digitalWrite(9,HIGH);
}
else if(t == 'w'){
  digitalWrite(9,LOW);
}
 
else if(t == 'S'){      //para os motores
  digitalWrite(13,LOW);
  digitalWrite(12,LOW);
  digitalWrite(11,LOW);
  digitalWrite(10,LOW);
}
delay(100);
}
