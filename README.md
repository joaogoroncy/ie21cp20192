# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|João Guilherme Lopes Goroncy|joaogoroncy|
|Bruno Gabriel Mayer Pereira|BrunoGMPereira|
|Tiago Jose Miguel|tiagomiguel16|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://joaogoroncy.gitlab.io/ie21cp20192/

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)