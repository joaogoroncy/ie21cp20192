# Carrinho de controle remoto

## Descrição
O presente trabalho tem o intuito de obtenção de nota na disciplina de 
Introdução a engenharia do curso de engenharia da computação da Universidade
Tecnológica Federal do Paraná, realizado pelos alunos João Guilherme Lopes 
Goroncy, Bruno Gabriel Mayer Pereira, Tiago Jose Miguel. Os quais produziram
um carrinho de controle remoto via bluetooth, utilizando Arduino.
    
## Materiais

* Arduino UNO
* HC-05 Bluetooth Module
* SparkFun Dual H – Bridge Motor Drivers L298
* Pimoroni Maker Essentials - Micro-motors & Grippy Wheels
* Jumper wires
* Li-lon Battery 1000mAh
* LED

## Esquema eletrico
O esquema eletrico pode ser visto abaixo:
![](Esquema.PNG)

## Obejetivos
O grupo tinha como objetivo produzir um carrinho de controle remoto via 
bluetooth utilizando um Arduino, onde a ideia surgiu no momento que um dos 
indivíduos do grupo estava buscando uma ideia interessante e achou o carrinho
realmente uma boa idea e de fácil realização, com um bom desenvolvimento.

## Resultados
O video demonstração pode ser visto em:
{{< youtube bkGG11PhrzM >}}
    
## Desafios encontrados
Os desafios encontrados, foram o de achar as peças, entender como elas 
funcionam e se localizão, como também fazer o algoritmo funcionar com os dois
motores. O circuito eletrico foi considerado um problema poís ocorreu uma
grande dificulade em montá lo e faze-lo funcionar de maneira correta, outro
problema foi o de achar um aplicativo de controle bluetooth compativel com a
modulo bluetooth anexado ao arduino.